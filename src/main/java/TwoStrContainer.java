import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class TwoStrContainer {
    public static class StrContainer implements Comparable<StrContainer> {
        private String first;
        private String second;

        public StrContainer(String _first, String _second) {
            first = _first;
            second = _second;
        }

        public String getFirst() {
            return first;
        }

        public String getSecond() {
            return second;
        }

        public int compareTo(StrContainer str) {
            //return first.compareTo(str.getFirst());
            return second.compareTo(str.getSecond());
        }

    }

    public static void generate(StrContainer[] array, List<StrContainer> list) {
        StrContainer first = new StrContainer("Kyiv", "Ukraine");
        array[0] = first;
        list.add(first);

        StrContainer secong = new StrContainer("Bucharest", "Romania");
        array[1] = secong;
        list.add(secong);

        StrContainer third = new StrContainer("Minsk", "Belarus");
        array[2] = third;
        list.add(third);

        StrContainer forth = new StrContainer("Warsaw", "Poland");
        array[3] = forth;
        list.add(forth);

        StrContainer fifth = new StrContainer("Berlin", "Germany");
        array[4] = fifth;
        list.add(fifth);

        StrContainer sixth = new StrContainer("Ottawa", "Canada");
        array[5] = sixth;
        list.add(sixth);
        }

    public static void main(String[] args) {
        StrContainer[] array = new StrContainer[10];
        List<StrContainer> list = new ArrayList<StrContainer>();
        generate(array, list);

        System.out.print("Unsorted array: ");
        for (StrContainer el : array) {
            if (el == null) {
                break;
            }
            System.out.print("\nCapital: " + el.getFirst() + "\tCountry: " + el.getSecond());
        }

        System.out.print("\nSorted array: ");

        try {
            Arrays.sort(array);
        }
        catch (NullPointerException ex){
        }

        for (StrContainer el : array) {
            if (el == null) {
                break;
            }
            System.out.print("\nCapital: " + el.getFirst() + "\tCountry: " + el.getSecond());
        }

        System.out.print("\n\nUnsorted ArrayList: ");
        for (StrContainer el : list) {
            System.out.print("\nCapital: " + el.getFirst() + "\tCountry: " + el.getSecond());
        }

        System.out.print("\nSorted ArrayList: ");

        Collections.sort(list);

        for (StrContainer el : array) {
            if(el==null){
                break;
            }
            System.out.print("\nCapital: " + el.getFirst() + "\tCountry: " + el.getSecond());
        }




    }
}
