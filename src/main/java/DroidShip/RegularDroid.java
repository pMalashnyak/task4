package DroidShip;

public class RegularDroid extends Droids {

    public RegularDroid(String name, int hp) {
        super(name, hp);
    }

    @Override public String toString() {
        return "I'm " + getName() + ",regular droid.\n";
    }
}
