package DroidShip;

import java.util.ArrayList;
import java.util.List;

public class Ship<T extends Droids> {
    private List<T> ship = new ArrayList<T>();

    public void add(T droid) {
        ship.add(droid);
    }

    public String remove(T droid) {
        ship.remove(droid);
        return (droid.getName() + " was removed.\n");
    }

    public void print() {
        for (Droids dr : ship) {
            System.out.print(dr.toString());
        }
    }
}
