package DroidShip;

public abstract class Droids {
    String name;
    int hp;

    public Droids(String _name, int _hp) {
        name = _name;
        hp = _hp;
    }

    public String getName() {
        return name;
    }

    public int getHp() {
        return hp;
    }

    public abstract String toString();
}
