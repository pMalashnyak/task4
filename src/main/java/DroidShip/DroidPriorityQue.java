package DroidShip;

public class DroidPriorityQue<T extends Droids> {
    private DroidQueModel<T>[] heap;
    private int heapSize;
    private int capacity;

    public DroidPriorityQue(int capacity) {
        this.capacity = capacity + 1;
        heap = new DroidQueModel[this.capacity];
        heapSize = 0;
    }

    public void clear() {
        heap = new DroidQueModel[capacity];
        heapSize = 0;
    }

    public boolean isEmpty() {
        return heapSize == 0;
    }

    public boolean isFull() {
        return heapSize == capacity - 1;
    }

    public int size() {
        return heapSize;
    }

    public void insert(T droid, int priority) {
        if (isFull()) {
            int newHeapSize = (int) (heapSize * 1.5);
            DroidQueModel<T>[] newHeap = new DroidQueModel[newHeapSize];
            int i = 0;
            for (DroidQueModel<T> dr : heap) {
                newHeap[i] = dr;
                ++i;
            }
            heap = newHeap;
        }

        DroidQueModel<T> newDroid = new DroidQueModel<T>(droid, priority);
        heap[++heapSize] = newDroid;
        int pos = heapSize;
        while (pos != 1 && newDroid.priority > heap[pos / 2].priority) {
            heap[pos] = heap[pos / 2];
            pos /= 2;
        }
        heap[pos] = newDroid;
    }

    public DroidQueModel<T> remove() {
        int parent, child;
        DroidQueModel<T> item;
        DroidQueModel<T> temp;
        if (isEmpty()) {
            System.out.println("Heap is empty.\n");
            return null;
        }

        item = heap[1];
        temp = heap[heapSize--];

        parent = 1;
        child = 2;
        while (child <= heapSize) {
            if (child < heapSize && heap[child].priority < heap[child + 1].priority)
                child++;
            if (temp.priority >= heap[child].priority)
                break;

            heap[parent] = heap[child];
            parent = child;
            child *= 2;
        }
        heap[parent] = temp;

        return item;
    }

    @Override public String toString() {
        String result = "";
        for (int i = 1; i <= heapSize; i++) {
            result += heap[i].toString();
        }
        return result;
    }
}
