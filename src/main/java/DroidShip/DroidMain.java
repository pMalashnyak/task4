package DroidShip;

public class DroidMain {
    public static void main(String[] args) {
        Ship<Droids> ship = new Ship<Droids>();
        ship.add(new BattleDroid("Ivan", 12));
        ship.add(new RegularDroid("Symon", 3));
        ship.add(new RepairDroid("C6-XD", 1));
        Droids Oleg=new BattleDroid("Oleg", 8);
        ship.add(Oleg);

        ship.print();

        System.out.print("\nRemove Oleg\n");
        System.out.print(ship.remove(Oleg));

        System.out.print("\nTesting PriorityQue\n");

        DroidPriorityQue<Droids> priorityQue=new DroidPriorityQue<Droids>(5);

        priorityQue.insert(new BattleDroid("Gen",44),12);
        priorityQue.insert(new RegularDroid("Len",4),1);
        priorityQue.insert(new RegularDroid("Fen",6),16);
        priorityQue.insert(new BattleDroid("Men",11),8);
        priorityQue.insert(new BattleDroid("Pen",12),2);
        priorityQue.insert(new BattleDroid("Pen",12),2);

        System.out.print(priorityQue.toString());

        priorityQue.remove();
        priorityQue.remove();
        priorityQue.remove();
        priorityQue.remove();
        priorityQue.remove();
        priorityQue.remove();
        priorityQue.remove();
        priorityQue.remove();


    }

}
