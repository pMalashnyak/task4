import java.util.ArrayList;
import java.util.List;

public class StringArray {
    public static class StrArray {
        private String[] array;

        public StrArray() {
            array = new String[5];
        }

        public StrArray(String[] _array) {
            array = _array;
        }

        public void add(String str) {
            int position = getInsertId();
            if (position >= array.length) {
                resize();
            }

            array[position] = str;
        }

        public void remove(int id) {
            String[] newArr = new String[array.length];
            for (int i = 0; i < array.length; ++i) {
                if (i != id) {
                    newArr[i] = array[i];
                }
            }
            array = newArr;
        }

        public String[] getArray() {
            return array;
        }

        private void resize() {
            String[] newArr = new String[array.length * 2];
            for (int i = 0; i < array.length; ++i) {
                newArr[i] = array[i];
            }

            array = newArr;
        }

        private int getInsertId() {
            int i = 0;
            for (String str : array) {
                if (str == null) {
                    return i;
                }
                ++i;
            }
            return i;
        }
    }

    public static void main(String[] args) {
        long startTime = System.nanoTime();
        StrArray arr = new StrArray();
        arr.add("First");
        arr.add("Second");
        arr.add("Third");
        arr.remove(2);

        long endTime = System.nanoTime();

        System.out.print("My array time: " + (endTime - startTime) + "\n");

        startTime=System.nanoTime();
        List<String> arr1=new ArrayList<String>();
        arr1.add("First");
        arr1.add("Second");
        arr1.add("Third");
        arr1.remove(2);

        endTime=System.nanoTime();

        System.out.print("ArrayList time: " + (endTime - startTime) + "\n");


    }
}
