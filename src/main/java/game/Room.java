package game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import game.Door.DoorModel;
import game.Hero.HeroModel;

public class Room {
    public static class RoomModel {
        private List<DoorModel> doors = new ArrayList<DoorModel>();
        private HeroModel hero = new HeroModel();
        private List<Integer> queDoors = new ArrayList<Integer>();

        public RoomModel() {
            for (int i = 0; i < 10; ++i) {
                doors.add(new DoorModel(i + 1));
            }
        }

        @Override public String toString() {
            String value = "";
            for (DoorModel door : doors) {
                value += doors.indexOf(door) + 1 + ": " + door.toString();
            }
            return value;
        }

        public String countDeathDoors() {
            int count = 0;
            for (DoorModel dr : doors) {
                if (!hero.fight(dr.getMonster())) {
                    count++;
                }
            }
            return "Death awaits you behind " + count + " doors.\n";
        }

        private void pickUpArtifacts() {
            for (DoorModel dr : doors) {
                if (dr.isMonster()) {
                    hero.addStrenght(dr.getArtifact());
                    queDoors.add(dr.number);
                }
            }
            System.out.print("Hero strength: "+hero.getStreght()+'\n');
        }


        private List<Monster.MonsterModel> sort() {
            List<Monster.MonsterModel> sorted = new ArrayList<Monster.MonsterModel>();
            for (DoorModel dr : doors) {
                if (!dr.isMonster()) {
                    sorted.add(dr.getMonster());
                }
            }
            Collections.sort(sorted);
            return sorted;
        }

        public void fightMonsters() {
            pickUpArtifacts();
            List<Monster.MonsterModel> monsters = sort();

            for (Monster.MonsterModel ms : monsters) {
                if (hero.fight(ms)) {
                    queDoors.add(getIndex(ms));
                } else {
                    System.out.print("You died!\n");
                }
            }
        }

        private int getIndex(Monster.MonsterModel ms) {
            for (DoorModel dr : doors) {
                if (dr.getMonster() == ms) {
                    return dr.number;
                }
            }
            return 0;
        }

        public void printQue() {
            for (Integer i : queDoors) {
                System.out.print(i + " ");
            }
            System.out.println();
        }
    }
}
