package game;

import java.util.Random;

public class Monster {
    public static class MonsterModel implements Comparable<MonsterModel>{
        private int strength;

        public MonsterModel() {
            Random rnd = new Random();
            strength = rnd.nextInt(96) + 5;
        }

        public int getStrenght() {
            return strength;
        }

        @Override public String toString() {
            return "Monster with " + strength + " strength\n";
        }

        public String getType() {
            return "Monster";
        }

        public int compareTo(MonsterModel o) {
            if(getStrenght()>o.getStrenght())
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }
    }
}
