package game;

import java.util.Random;

import game.Monster.MonsterModel;
import game.Artifact.ArtifactModel;

public class Door {
    public static class DoorModel implements Comparable<DoorModel> {
        private MonsterModel monster;
        private ArtifactModel artifact;
        int number;

        public DoorModel(int num) {
            Random rnd = new Random();
            if (rnd.nextDouble() >= 0.5) {
                monster = new MonsterModel();
            } else {
                artifact = new ArtifactModel();
            }
            number = num;
        }

        @Override public String toString() {
            if (monster == null) {
                return artifact.toString();
            } else {
                return monster.toString();
            }
        }

        public int getNumber(MonsterModel ms){
            if(ms==monster){
                return number;
            }
            return 0;
        }

        public int compareTo(DoorModel anotherDoor) {
            if (isMonster()||anotherDoor.isMonster()) {
                return -1;
            }
            return monster.compareTo(anotherDoor.monster);
        }

        public MonsterModel getMonster() {
            return monster;
        }

        public ArtifactModel getArtifact() {
            return artifact;
        }

        public boolean isMonster() {
            return (monster == null);
        }
    }
}
