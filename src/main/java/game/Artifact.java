package game;

import java.util.Random;

public class Artifact {
    public static class ArtifactModel {
        private int strength;

        public ArtifactModel() {
            Random rnd = new Random();
            strength = rnd.nextInt(71) + 10;
        }

        public int getStrength() {
            return strength;
        }

        @Override public String toString() {
            return "Artifact with " + strength + " strength\n";
        }

        public String getType() {
            return "Artifact";
        }
    }
}
