package logicalTasks;


import org.omg.Messaging.SYNC_WITH_TRANSPORT;

public class TwoArrays {
    String[] first = new String[20];
    String[] second = new String[20];

    public TwoArrays(String[] f, String[] s) {
        first = f;
        second = s;
    }

    private final int findLast(String[] arr) {
        for (int i = arr.length - 1; i > 0; --i) {
            if (arr[i] != null) {
                return i;
            }
        }
        return 0;
    }

    public String[] getAll() {
        int length;
        if (first.length > second.length) {
            length = first.length;
        } else {
            length = second.length;
        }
        String[] result = new String[length];

        int i = 0;
        int fLast = findLast(first);
        int sLast = findLast(second);
        for (int j = 0; j <= fLast; ++j) {
            for (int k = 0; k <= sLast; ++k) {
                if (first[j] == second[k]) {
                    result[i] = first[j];
                    i++;
                }
            }
        }
        return result;
    }

    public void print() {
        System.out.print("First array: ");
        for (int i = 0; i <= findLast(first); ++i) {
            System.out.print(first[i] + " ");
        }
        System.out.print("\nSecond array: ");
        for (int i = 0; i <= findLast(second); ++i) {
            System.out.print(second[i] + " ");
        }
    }

    public String[] getDistinct(int num) {

        if (num == 1) {
            return getUnique(first, second);
        } else {
            return getUnique(second, first);
        }
    }

    private String[] getUnique(String[] arr, String[] sarr) {
        String[] result = new String[arr.length];
        int k = 0;
        for (int j = 0; j <= findLast(arr); j++) {
            String thisT = arr[j];

            boolean seenThisBefore = false;
            for (int i = 0; i <= findLast(sarr); i++) {
                if (thisT == sarr[i]) {
                    seenThisBefore = true;
                }
            }

            if (!seenThisBefore) {
                result[k] = thisT;
                k++;
            }
        }

        return result;
    }
}
