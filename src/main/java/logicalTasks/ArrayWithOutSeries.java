package logicalTasks;

public class ArrayWithOutSeries {
    private Integer[] array = new Integer[20];

    public ArrayWithOutSeries(Integer[] _array) {
        array = _array;
    }

    private final int findLast(Integer[] arr) {
        for (int i = arr.length - 1; i > 0; --i) {
            if (arr[i] != null) {
                return i;
            }
        }
        return 0;
    }

    public void filter() {
        Integer[] result = new Integer[array.length];
        int last = findLast(array);
        int k = 0;
        for (int i = 0; i <= last; i++) {
            result[k] = array[i];
            while (i + 1 <= last && i <= last && array[i] == array[i + 1]) {
                i++;
            }
            k++;
        }

        array = result;
    }

    public void print() {
        for (Integer i : array) {
            if (i == null) {
                break;
            }
            System.out.print(i + " ");
        }
        System.out.println();
    }
}
